# rc-project

#### 介绍
精准决策数字底板系统，前后端分离项目，后台采用springboot，mybatis-plus，mysql数据库，caffeine，swagger2, shiro,jwt,logback等技术栈，前端采用vue，ant-design-vue，axios，echarts，element-ui等技术栈，实现的一整套精准决策数字底板系统。精美的ui设计，接入北京睿呈三维地图产品，该项目针对城市管理者等用户，在社区管理层面全方位的统计并展示了城市区域的人口统计，法人信息，区域信息，事件信息，房产类型，房屋使用情况等监控指标，在棚屋改造层面全方位的统计并展示了楼栋，基础设施，监控视频等指标。

#### 软件架构
软件架构说明
后台采用springboot，mybatis-plus，mysql数据库，caffeine，swagger2, shiro,jwt,logback等技术栈，前端采用vue，ant-design-vue，axios，echarts，element-ui等技术栈

#### 安装教程
前端项目启动：
cnpm install
cnpm run serve


后台项目启动：
安装maven依赖，直接在idea中找到启动类点击run或者debug按钮


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

![输入图片说明](https://images.gitee.com/uploads/images/2020/1013/154400_fbb4dd11_1677589.png "IMG20201013_154338.png")