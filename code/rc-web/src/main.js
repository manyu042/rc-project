import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
// import ElementUI from 'element-ui';
import '@src/assets/css/common.less';
// import 'element-ui/lib/theme-chalk/index.css';
import { message ,Tree ,Icon , Checkbox ,Button ,Modal} from 'ant-design-vue'
import mapService from '@src/utils/mapService'
mapService.getMessageData();
// Vue.use(ElementUI)
Vue.component(Tree.name, Tree);
Vue.component(Icon.name, Icon);
Vue.component(Checkbox.name, Checkbox);
Vue.component(Button.name, Button);
Vue.component(Modal.name, Modal);
Vue.prototype.$message = message;
Vue.config.productionTip = false;
// 定义一个全局混合对象
Vue.mixin({
  data() {
    return {
      // 行内样式适配使用
      desgin1pxToCilent:(1/process.env.VUE_APP_ViewportWidth) * document.body.clientWidth,
      // publicPath:process.env.BASE_URL
    }
  },
  methods: {

  },
})
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");

