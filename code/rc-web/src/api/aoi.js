import request from '@/utils/axios.config'

export function getHouseRebuildInfo(aoi_id) {
  return request({
    url: '/house-rebuild/getHouseByAoiId',
    method: 'get',
    params: aoi_id
  })
}
export function getHouseRebuildAOITree (aoi_id){
  return request({
    url: '/house-rebuild/getHouseTreeByAoiId',
    method: 'get',
    params: aoi_id
  })
}
export function getCommunityStatistInfo(aoi_id) {
  return request({
    url: '/community-statist/getCommunityByAoiId',
    method: 'get',
    params: aoi_id 
  })
}
export function getCommunityAOITree(aoi_id) {
  return request({
    url: '/community-statist/getCommunityTreeByAoiId',
    method: 'get',
    params: aoi_id 
  })
}
export function getAOIInfo(params) {
  /**
    area_id:aoi_id,
    adcode:440300
  */
  return request({
    url: 'http://10.82.244.103:8096/sf3d/area/info',
    method: 'get',
    params: params 
  })
}
export function getRebuildAOIInfo(params) {
  /**
    aoi_id:aoi_id,
  */
  return request({
    url: '/house-rebuild-device/getDeviceByAoiId',
    method: 'get',
    params: params 
  })
}