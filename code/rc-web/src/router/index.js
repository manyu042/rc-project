import Vue from "vue";
import VueRouter from "vue-router";
import Home5670 from "../views/Home/5670/home";
import Home from "../views/Home/home";
import Test from "../views/Test/test";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/home",
    name: "Home",
    component: Home,
  },
  {
    path: "/home5670",
    name: "Home5670",
    component: Home5670,
  },
  {
    path: "/test",
    name: "test",
    component: Test,
  },
  // {
  //   path: "/about",
  //   name: "About",
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () =>
  //     import(/* webpackChunkName: "about" */ "../views/About.vue")
  // }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
