const mapService ={
	currentMsg:null,
	fnNameMap:new Map(),
	sendService:function(message){
		//获取发送地址
		this.currentMsg=message;
		var address = document.getElementById("3dcharts").src;
		var sendMsg ={
			service: this.getFnName(message)
		}
		//发送命令，指令为JSON字符串格式
		window.frames[0].postMessage(JSON.stringify(sendMsg),address);
	},
	getFnName:function(obj){//获取回调函数名称
		let result = Array.isArray(obj) ? [] : {};
        for (let key in obj) {
            if (Object.prototype.hasOwnProperty.call(obj,key)) {
                if (typeof obj[key] === 'object' && obj[key]!==null) {
                    result[key] = this.getFnName(obj[key]);   //递归复制
                } else {
                    if(key == "function_name"){
												this.fnNameMap.set(this.currentMsg.effect_id,obj[key]);
                        result[key] = "getData";
                    }else{
                        result[key] = obj[key];
                    }
                }
            }
        }
        return result;
	},
	getMessageData:function(){
		let that = this;
		//监听回调事件
		window.addEventListener('message',function(e){
            if(e.data["type"] == "status"){
				  try{
					if(that.get3dMapStatus && typeof(that.get3dMapStatus) == "function"){
						that.get3dMapStatus(e.data.serviceStatus);
					}
				  }catch(e){
					console.log("获取状态方法不存在");
					}
            }else{
							var funcName = that.fnNameMap.get(e.data.effect_id);
							//获取命令回调名称并执行返回参数
							if(typeof(eval(funcName)) == "function"){
								var fnName = eval(funcName);
								new fnName(e.data);
							}else{
								console.log("回调函数不存在");
							}
            }
		},false);
	},
	getServiceStatus:function(){
		window.addEventListener('message',function(e){
			console.log(e);
		},false);
	},
	get3dMapStatus:function(){

	}
}
export default mapService

	 
