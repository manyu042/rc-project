const state = {
  videoList:[],
  currentHandleclickVideo:null
}

const mutations = {
  SET_VIDEOLIST: (state, videoList) => {
    state.videoList = videoList
  },
  SET_CURRENT_HANDLE_CLICK_VIDEO: (state, currentHandleclickVideo) => {
    let arr =  state.videoList.filter(el=>el.effect_id==currentHandleclickVideo);
    if(arr.length>0){
      state.currentHandleclickVideo =arr[0];
    }else{
      state.currentHandleclickVideo=null;
    }
  },
  
}

const actions = {
  setVideoList({ commit },videoList){
    commit('SET_VIDEOLIST', videoList)
  },
  setCurrentHandleClickVideo({ commit },currentHandleclickVideo){
    commit('SET_CURRENT_HANDLE_CLICK_VIDEO', currentHandleclickVideo)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
