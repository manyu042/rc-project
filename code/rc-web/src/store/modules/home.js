const state = {
  currentNav: '',
  // communityData:{
  //   aoiStatist: {
  //     aoi: null,
  //     aoiId: null,
  //     business: null,
  //     cityManage: null,
  //     community: null,
  //     createTime: null,
  //     eliminateEvil: null,
  //     enterprise: null,
  //     factoryBuilding: null,
  //     foreignPeople: null,
  //     grid: null,
  //     hkmtPeople: null,
  //     house: null,
  //     houseTypeOther: null,
  //     houseUseOther: null,
  //     indBusiness: null,
  //     keepEvent: null,
  //     laborDispute: null,
  //     lease: null,
  //     mark: null,
  //     multiple: null,
  //     noRegisPeople: null,
  //     office: null,
  //     partRent: null,
  //     publicFac: null,
  //     regisPeople: null,
  //     road: null,
  //     selfUse: null,
  //     shape: null,
  //     store: null,
  //     vacant: null,
  //     waitRent: null,
  //   },
  //   farenTotal:null,
  //   houseTypeTotal: null,
  //   houseUseTotal: null,
  //   peopleTotal: null,
  //   eventTotal:4
  // },
  communityData:null,
  reBuildData:null,
  headerAndControlVisible:false,
  sceneBtnVisible:false,
  sceneIframeVisible:false,
  sceneSummaryVisible:false
}

const mutations = {
  SET_CURRENTNAV: (state, currentNav) => {
    state.currentNav = currentNav
  },
  SET_COMMUNITY_DATA:(state, communityData) =>{
    state.communityData = communityData
  },
  SET_RE_BUILD_DATA:(state, reBuildData) =>{
    state.reBuildData = reBuildData
  },
  SET_HEADER_AND_CONTROL_VISIBLE:(state, headerAndControlVisible) => {
    state.headerAndControlVisible = headerAndControlVisible
  },
  SET_SCENE_BTN_VISIBLE:(state, sceneBtnVisible) => {
    state.sceneBtnVisible = sceneBtnVisible
  },
  SET_SCENE_IFRAME_VISIBLE:(state, sceneIframeVisible) => {
    state.sceneIframeVisible = sceneIframeVisible
  },
  SET_SCENE_SUMMARY_VISIBLE:(state, sceneSummaryVisible) => {
    state.sceneSummaryVisible = sceneSummaryVisible
  },
}

const actions = {
  setCurrentNav({ commit }, currentNav) {
    commit('SET_CURRENTNAV', currentNav)
  },
  setCommunityData({ commit }, communityData){
    commit('SET_COMMUNITY_DATA', communityData)
  },
  setReBuildData({ commit }, reBuildData){
    commit('SET_RE_BUILD_DATA', reBuildData)
  },
  setHeaderAndControlVisible({ commit }, headerAndControlVisible) {
    commit('SET_HEADER_AND_CONTROL_VISIBLE', headerAndControlVisible)
  },
  setSceneBtnVisible({ commit }, sceneBtnVisible) {
    commit('SET_SCENE_BTN_VISIBLE', sceneBtnVisible)
  },
  setSceneIframeVisible({ commit }, sceneIframeVisible) {
    commit('SET_SCENE_IFRAME_VISIBLE', sceneIframeVisible)
  },
  setSceneSummaryVisible({ commit }, sceneSummaryVisible) {
    commit('SET_SCENE_SUMMARY_VISIBLE', sceneSummaryVisible)
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
