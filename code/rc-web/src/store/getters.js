const getters = {
  currentNav: state => state.home.currentNav,
  communityData: state => state.home.communityData,
  reBuildData: state => state.home.reBuildData,
  headerAndControlVisible: state => state.home.headerAndControlVisible,
  sceneBtnVisible: state => state.home.sceneBtnVisible,
  sceneIframeVisible: state => state.home.sceneIframeVisible,
  sceneSummaryVisible: state => state.home.sceneSummaryVisible,

  // video
  videoList: state => state.video.videoList,
  currentHandleclickVideo:state => state.video.currentHandleclickVideo

}
export default getters
