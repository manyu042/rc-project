export const treeData = {
  enable: true,
  name: "图层管理",
  id: "ROOT",
  layers: [
    {
      id: "EAA50A88-DDFA-4114-8C2C-227575B80BA2",
      name: "影像层",
      type: "folder",
      dtype: "folder",
      enable: true,
      layers: [
        {
          id: "EAA50A88-DDFA-4124-8C2C-227555B80BA5",
          name: "全球影像",
          enable: true,
          url: "http://10.202.116.68:8090/geoserver/gwc/service/wmts?layer=yt_sata%3Aworld&style=&tilematrixset=EPSG%3A900913&Service=WMTS&Request=GetTile&Version=1.0.0&Format=image%2Fvnd.jpeg-png&TileMatrix=EPSG%3A900913%3A{z}&TileCol={x}&TileRow={y}",
          maximumLevel: 19,
          minimumLevel: 1,
          type: "maptiles",
          dtype: "maptiles",
          rect: ""
        },
        {
          id: "EAA50A88-DDFA-4125-8C2C-227555B80BA5",
          name: "中国影像",
          enable: true,
          url: "http://10.202.116.68:8090/geoserver/gwc/service/wmts?layer=yt_sata%3Achina_12&style=&tilematrixset=EPSG%3A900913&Service=WMTS&Request=GetTile&Version=1.0.0&Format=image%2Fvnd.jpeg-png&TileMatrix=EPSG%3A900913%3A{z}&TileCol={x}&TileRow={y}",
          maximumLevel: 19,
          minimumLevel: 1,
          type: "maptiles",
          dtype: "maptiles",
          rect: ""
        },
        {
          id: "EAA50A88-DDFA-4126-8C2C-227555B80BA5",
          name: "深圳影像",
          enable: true,
          url: "http://10.202.116.68:8090/geoserver/gwc/service/wmts?layer=yt_sata%3Ashenzhen&style=&tilematrixset=EPSG%3A900913&Service=WMTS&Request=GetTile&Version=1.0.0&Format=image%2Fvnd.jpeg-png&TileMatrix=EPSG%3A900913%3A{z}&TileCol={x}&TileRow={y}",
          maximumLevel: 19,
          minimumLevel: 1,
          type: "maptiles",
          dtype: "maptiles",
          rect: ""
        },
        {
          id: "EAA50A88-DDFA-4114-8C2C-227575B80BA3",
          name: "MapBox卫星瓦片",
          enable: false,
          url: "//c.tiles.mapbox.com/v4/mapbox.satellite/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiYW5hbHl0aWNhbGdyYXBoaWNzIiwiYSI6ImNpd204Zm4wejAwNzYyeW5uNjYyZmFwdWEifQ.7i-VIZZWX8pd1bTfxIVj9g",
          brightness: 1,
          maximumLevel: 12,
          minimumLevel: 1,
          type: "maptiles",
          dtype: "maptiles",
          rect: ""
        },
        {
          id: "EAA50A88-DDFA-4114-8C2C-227575B80BA4",
          name: "顺丰蓝色瓦片",
          enable: false,
          url: "https://webmap-tile.sf-express.com/MapTileService/maptile?fetchtype=static&x={x}&y={y}&z={z}&project=sfmap&pic_size=256&pic_type=png8&data_name=hnight&data_format=merged-dat&data_type=normal",
          srcCoordType: "GCJ02",
          dstCoordType: "WGS84",
          maximumLevel: 19,
          minimumLevel: 1,
          type: "maptiles",
          dtype: "maptiles",
          rect: ""
        },
        {
          id: "EAA50A88-DDFA-4114-8C2C-227555B80BA5",
          name: "盐田精细",
          enable: true,
          url: "http://10.202.116.68:8090/geoserver/gwc/service/wmts?layer=yt_sata%3Ayt_dom&style=&tilematrixset=EPSG%3A900913&Service=WMTS&Request=GetTile&Version=1.0.0&Format=image%2Fvnd.jpeg-png&TileMatrix=EPSG%3A900913%3A{z}&TileCol={x}&TileRow={y}",
          maximumLevel: 19,
          minimumLevel: 1,
          type: "maptiles",
          dtype: "maptiles",
          rect: ""
        },
        {
          id: "EAA50A88-DDFA-4114-8C2C-227555B80BB1",
          name: "盐田现场",
          enable: false,
          url: "http://10.21.232.23:50001/proxy/server/AF8CF1CE70A8460B85A2D4155AF755DB/25A0E34254A04FCFB2E3276924A03615/tile/{TileMatrix}/{TileRow}/{TileCol}",
          maximumLevel: 19,
          minimumLevel: 1,
          type: "maptiles",
          dtype: "maptiles",
          rect: ""
        },
        {
          id: "EAA50A88-DDFA-4116-8C2C-227555B80BB1",
          name: "盐田day",
          enable: false,
          url: "http://10.202.116.67:8090/MapTileService/wmts?SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&STYLE=default&STORETYPE=merged-dat&LAYER=wmts_4326_440300&PROJECTION=4326&TILEMATRIXSET=c&TILEMATRIX={TileMatrix}&TILEROW={TileRow}&TILECOL={TileCol}&FORMAT=image/png",
          srcCoordType: "WGS84",
          dstCoordType: "WGS84",
          tileMatrixSetID: "EPSG:4326",
          tileMatrixLabels: [
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
            15,
            16,
            17,
            18,
            19
          ],
          maximumLevel: 19,
          minimumLevel: 1,
          type: "wmts",
          dtype: "wmts",
          rect: ""
        }
      ]
    },
    {
      id: "EAA50A88-DDFA-4114-8C2C-227575B81BA2",
      name: "地形层",
      type: "folder",
      dtype: "folder",
      enable: true,
      layers: [
        {
          id: "EAA50A88-DDFA-4114-8C2C-227575B80BA6",
          name: "地形",
          enable: true,
          url: "http://10.82.244.103/440300/dem",
          type: "terrain",
          dtype: "terrain",
          rect: "",
          maximumLevel: 24,
          minimumLevel: 13
        }
      ]
    },
    {
      id: "EAA50A88-DDFA-4114-8C2C-227575B82BA2",
      name: "模型层",
      type: "folder",
      dtype: "folder",
      enable: true,
      layers: [
        {
          id: "EAA50A88-DDFA-4114-8C2C-227575B80BAA",
          name: "盐田倾斜",
          enable: true,
          url: "/440300/YTALL128/tileset.json",
          type: "op",
          dtype: "tilesets",
          rect: "",
          maximumLevel: 24,
          minimumLevel: 13
        }
      ]
    },
    {
      id: "EAA50A88-DDFA-4114-8C2C-227575B83BA2",
      name: "路网层",
      type: "folder",
      dtype: "folder",
      enable: true,
      layers: [
        {
          id: "EAA50A88-DDFA-4114-8C2C-227575B80BA7",
          name: "道路",
          enable: false,
          url: ":8080/sf3d/city/440300/road.json",
          type: "road",
          dtype: "geojson",
          rect: "",
          maximumLevel: 24,
          minimumLevel: 13
        }
      ]
    },
    {
      id: "EAA50A88-DDFA-4114-8C2C-227575B84BA2",
      name: "背景层",
      type: "folder",
      dtype: "folder",
      enable: true,
      layers: [
        {
          id: "EAA50A88-DDFA-4114-8C2C-227575B80BA8",
          name: "水系",
          enable: false,
          url: "/440300/water_yt/water_yt.json",
          type: "waterfield",
          dtype: "geojson",
          rect: "",
          maximumLevel: 24,
          minimumLevel: 13
        },
        {
          id: "EAA50A88-DDFA-4114-8C2C-227575B80BA9",
          name: "绿地",
          enable: false,
          url: ":8080/sf3d/city/440300/greenfield.json",
          type: "greenfield",
          dtype: "geojson",
          rect: "",
          maximumLevel: 24,
          minimumLevel: 13
        }
      ]
    }
  ]
}
