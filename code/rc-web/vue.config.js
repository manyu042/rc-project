const path = require("path");
 
function resolve(dir) {
  return path.join(__dirname, dir);
}

module.exports = {
  publicPath: process.env.NODE_ENV === 'production'?'./':'/',
  // 输出文件目录
  outputDir: '../rc-boot/src/main/resources/static',
  assetsDir: "static",
  // 生产环境sourceMap
  productionSourceMap: true,
  configureWebpack: {
    // provide the app's title in webpack's name field, so that
    // it can be accessed in index.html to inject the correct title.
    resolve: {
      // 配置别名
      alias: {
        "@src": resolve("src")
      }
    }
  },
  css: {
    loaderOptions: {
      postcss: {
        plugins: (loader) => [
          require('postcss-px-to-viewport')({
            // 设计图5760
            viewportWidth: process.env.VUE_APP_ViewportWidth, // (Number) 设计稿的视口宽度
            unitPrecision: 4, // (Number) 单位转换后保留的精度
            viewportUnit: 'vw', // (String) 希望使用的视口单位.
            fontViewportUnit: 'vw', // 字体使用的视口单位
            selectorBlackList: ['.ignore', '.hairlines'], 
            minPixelValue: 1, // (Number)  设置最小的转换数值，如果为1的话，只有大于1的值会被转换
            mediaQuery: false, // (Boolean)  媒体查询里的单位是否需要转换单位.
            replace: true, // (Boolean) 是否直接更换属性值，而不添加备用属性
            exclude: [], //  (Array or Regexp) 忽略某些文件夹下的文件或特定文件，例如 'node_modules' 下的文件
            landscape: false, // (Boolean) 是否添加根据 landscapeWidth 生成的媒体查询条件 @media (orientation: landscape)
          })
        ]
      }
    }
  },
  // webpack-dev-server 相关配置
  devServer: {
    // host: 'localhost',
    host:'10.82.249.1',
    port: 8080,
    proxy: 'http://localhost:8085',
  },
}