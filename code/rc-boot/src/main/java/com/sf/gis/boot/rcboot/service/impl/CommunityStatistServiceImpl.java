package com.sf.gis.boot.rcboot.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sf.gis.boot.rcboot.entity.CameraCoordinate;
import com.sf.gis.boot.rcboot.entity.CommunityStatist;
import com.sf.gis.boot.rcboot.entity.PositionCoordinate;
import com.sf.gis.boot.rcboot.mapper.CommunityStatistMapper;
import com.sf.gis.boot.rcboot.service.ICommunityStatistService;
import com.sf.gis.boot.rcboot.vo.CommunityStatistVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 社区管理表 服务实现类
 * </p>
 *
 * @author 段朝旭
 * @since 2020-09-11
 */
@Service
public class CommunityStatistServiceImpl extends ServiceImpl<CommunityStatistMapper, CommunityStatist> implements ICommunityStatistService {

    @Autowired
    private CommunityStatistMapper communityStatistMapper;

    @Override
    public CommunityStatist getByAoiId(String aoiId) {
        CommunityStatist communityStatist = communityStatistMapper.getByAoiId(aoiId);
        this.setCoord(communityStatist);
        return communityStatist;
    }

    @Override
    public LinkedHashMap<String, Object> staticCommunity(String aoiId) {
        return communityStatistMapper.staticCommunity(aoiId);
    }

    @Override
    public CommunityStatistVo getCommunityTreeByAoiId(String aoiId) {
        //获取当前节点
        CommunityStatist root = this.getByAoiId(aoiId);
        CommunityStatistVo rootVo = new CommunityStatistVo();
        if (root != null) {
            BeanUtils.copyProperties(root, rootVo);
            rootVo = getCommunityTree(rootVo);
        }
        //转换成vo
        return rootVo;
    }

    @Override
    public List<CommunityStatist> getCommunityByParentAoiId(String parentAoiId) {
        List<CommunityStatist> communityStatistList = communityStatistMapper.getByParentAoiId(parentAoiId);
        communityStatistList.forEach(this::setCoord);
        return communityStatistList;
    }

    /**
     * 组装社区管理树形结构
     *
     * @param rootVo
     * @return
     */
    private CommunityStatistVo getCommunityTree(CommunityStatistVo rootVo) {
        String aoiId = rootVo.getAoiId();
        List<CommunityStatistVo> children = this.getCommunityByParentAoiId(aoiId).stream().map(x -> {
                    CommunityStatistVo communityStatistVo = new CommunityStatistVo();
                    BeanUtils.copyProperties(x, communityStatistVo);
                    return communityStatistVo;
                }
        ).collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(children)) {
            rootVo.getChildren().addAll(children);
            for (CommunityStatistVo node : children) {
                getCommunityTree(node);
            }
        }
        return rootVo;
    }

    /**
     * 设置坐标角度信息
     *
     * @param communityStatist
     */
    private void setCoord(CommunityStatist communityStatist) {
        //设置真实定位坐标
        BigDecimal posX = communityStatist.getPosX();
        BigDecimal posY = communityStatist.getPosY();
        BigDecimal posZ = communityStatist.getPosZ();
        PositionCoordinate positionCoordinate = new PositionCoordinate(posX, posY, posZ);
        communityStatist.setPositionCoordinate(positionCoordinate);

        //设置相机定位坐标和角度
        BigDecimal x = communityStatist.getX();
        BigDecimal y = communityStatist.getY();
        BigDecimal z = communityStatist.getZ();
        BigDecimal course = communityStatist.getCourse();
        BigDecimal alpha = communityStatist.getAlpha();
        BigDecimal roll = communityStatist.getRoll();
        CameraCoordinate cameraCoordinate = new CameraCoordinate(x, y, z, course, alpha, roll);
        communityStatist.setCameraCoordinate(cameraCoordinate);
    }

}
