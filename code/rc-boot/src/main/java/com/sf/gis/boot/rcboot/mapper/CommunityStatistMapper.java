package com.sf.gis.boot.rcboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sf.gis.boot.rcboot.entity.CommunityStatist;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * <p>
 * 社区管理表 Mapper 接口
 * </p>
 *
 * @author 段朝旭
 * @since 2020-09-11
 */
@Mapper
@Repository
public interface CommunityStatistMapper extends BaseMapper<CommunityStatist> {


    LinkedHashMap<String, Object> staticCommunity(String aoiId);

    CommunityStatist getByAoiId(String aoiId);

    List<CommunityStatist> getByParentAoiId(String parentAoiId);
}
