package com.sf.gis.boot.rcboot.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 社区管理表
 * </p>
 *
 * @author 段朝旭
 * @since 2020-09-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "CommunityStatist对象", description = "社区管理表")
@TableName("community_statist")
public class CommunityStatist implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "aoi_id")
    private String aoiId;

    @TableField("aoi")
    private String aoi;

    @TableField(value = "shape")
    private String shape;

    @TableField(value = "shape_text")
    private String shapeText;

    @TableField("create_time")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "户籍人口")
    @TableField("regis_people")
    private Integer regisPeople;

    @ApiModelProperty(value = "非户籍人口")
    @TableField("no_regis_people")
    private Integer noRegisPeople;

    @ApiModelProperty(value = "港澳台人口")
    @TableField("hkmt_people")
    private Integer hkmtPeople;

    @ApiModelProperty(value = "外籍人口")
    @TableField("foreign_people")
    private Integer foreignPeople;

    @ApiModelProperty(value = "企业单位")
    @TableField("enterprise")
    private Integer enterprise;

    @ApiModelProperty(value = "个体工商户")
    @TableField("ind_business")
    private Integer indBusiness;

    @ApiModelProperty(value = "小区")
    @TableField("community")
    private Integer community;

    @ApiModelProperty(value = "道路街巷")
    @TableField("road")
    private Integer road;

    @ApiModelProperty(value = "标注物")
    @TableField("mark")
    private Integer mark;

    @ApiModelProperty(value = "网格")
    @TableField("grid")
    private Integer grid;

    @ApiModelProperty(value = "扫黑除恶")
    @TableField("eliminate_evil")
    private Integer eliminateEvil;

    @ApiModelProperty(value = "维稳事件")
    @TableField("keep_event")
    private Integer keepEvent;

    @ApiModelProperty(value = "城市管理")
    @TableField("city_manage")
    private Integer cityManage;

    @ApiModelProperty(value = "劳资纠纷")
    @TableField("labor_dispute")
    private Integer laborDispute;

    @ApiModelProperty(value = "综合")
    @TableField("multiple")
    private Integer multiple;

    @ApiModelProperty(value = "住宅")
    @TableField("house")
    private Integer house;

    @ApiModelProperty(value = "商业")
    @TableField("business")
    private Integer business;

    @ApiModelProperty(value = "厂房")
    @TableField("factory_building")
    private Integer factoryBuilding;

    @ApiModelProperty(value = "仓库")
    @TableField("store")
    private Integer store;

    @ApiModelProperty(value = "办公")
    @TableField("office")
    private Integer office;

    @ApiModelProperty(value = "公共设施")
    @TableField("public_fac")
    private Integer publicFac;

    @ApiModelProperty(value = "房屋类型其他")
    @TableField("house_type_other")
    private Integer houseTypeOther;

    @ApiModelProperty(value = "自用")
    @TableField("self_use")
    private Integer selfUse;

    @ApiModelProperty(value = "出租")
    @TableField("lease")
    private Integer lease;

    @ApiModelProperty(value = "空置")
    @TableField("vacant")
    private Integer vacant;

    @ApiModelProperty(value = "待租")
    @TableField("wait_rent")
    private Integer waitRent;

    @ApiModelProperty(value = "部分出租")
    @TableField("part_rent")
    private Integer partRent;

    @ApiModelProperty(value = "房屋使用情况其他")
    @TableField("house_use_other")
    private Integer houseUseOther;

    @ApiModelProperty(value = "相机坐标，角度")
    @TableField(value = "cameraCoordinate", exist = false)
    private CameraCoordinate cameraCoordinate;

    @ApiModelProperty(value = "定位坐标")
    @TableField(value = "positionCoordinate", exist = false)
    private PositionCoordinate positionCoordinate;

    @ApiModelProperty(value = "相机经度")
    @TableField("x")
    @JsonIgnore
    private BigDecimal x;

    @ApiModelProperty(value = "相机纬度")
    @TableField("y")
    @JsonIgnore
    private BigDecimal y;

    @ApiModelProperty(value = "相机z轴")
    @TableField("z")
    @JsonIgnore
    private BigDecimal z;

    @ApiModelProperty(value = "相机俯仰角")
    @TableField("course")
    @JsonIgnore
    private BigDecimal course;

    @ApiModelProperty(value = "相机方位角")
    @TableField("alpha")
    @JsonIgnore
    private BigDecimal alpha;

    @ApiModelProperty(value = "相机滚转角")
    @TableField("roll")
    @JsonIgnore
    private BigDecimal roll;

    @ApiModelProperty(value = "定位经度")
    @TableField("pos_x")
    @JsonIgnore
    private BigDecimal posX;

    @ApiModelProperty(value = "定位纬度")
    @TableField("pos_y")
    @JsonIgnore
    private BigDecimal posY;

    @ApiModelProperty(value = "定位z轴")
    @TableField("pos_z")
    @JsonIgnore
    private BigDecimal posZ;


}
