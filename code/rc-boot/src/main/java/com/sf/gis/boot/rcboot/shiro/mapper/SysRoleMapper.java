package com.sf.gis.boot.rcboot.shiro.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.sf.gis.boot.rcboot.shiro.entity.SysRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 系统角色表 Mapper 接口
 * </p>
 */
@Repository
@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole> {

    List<SysRole> findUserRole(@Param("username") String username);

}
