package com.sf.gis.boot.rcboot.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
@Api(tags = "页面跳转接口")
@Slf4j
public class PageBaseController {

    @GetMapping("/")
    @ApiOperation("请求/重定向到index.html")
    public String index1() {
        return "forward:/index.html";
    }

    @GetMapping("/index")
    @ApiOperation("请求/index重定向到index.html")
    public String index2() {
        return "forward:/index.html";
    }

}
