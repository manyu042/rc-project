package com.sf.gis.boot.rcboot.service.impl;

import com.sf.gis.boot.rcboot.entity.MoveInOut;
import com.sf.gis.boot.rcboot.mapper.MoveInOutMapper;
import com.sf.gis.boot.rcboot.service.IMoveInOutService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 迁入迁出情况表 服务实现类
 * </p>
 *
 * @author 段朝旭
 * @since 2020-09-10
 */
@Service
public class MoveInOutServiceImpl extends ServiceImpl<MoveInOutMapper, MoveInOut> implements IMoveInOutService {

}
