package com.sf.gis.boot.rcboot.shiro.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.sf.gis.boot.rcboot.shiro.entity.SysUserRole;
import com.sf.gis.boot.rcboot.shiro.mapper.SysUserRoleMapper;
import com.sf.gis.boot.rcboot.shiro.service.SysUserRoleService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 用户角色关系表 服务实现类
 * </p>
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements SysUserRoleService {

    @Override
    public void deleteUserRolesByUsername(String username) {

        QueryWrapper<SysUserRole> deleteWrapper = new QueryWrapper<>();
        deleteWrapper.eq("username", username);
        this.baseMapper.delete(deleteWrapper);
    }

    @Override
    public List<SysUserRole> listByUsername(String username) {
        QueryWrapper<SysUserRole> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("username", username);
        return this.baseMapper.selectList(queryWrapper);
    }
}
