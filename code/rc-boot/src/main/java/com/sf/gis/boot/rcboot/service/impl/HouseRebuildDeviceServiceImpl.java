package com.sf.gis.boot.rcboot.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sf.gis.boot.rcboot.entity.HouseRebuildDevice;
import com.sf.gis.boot.rcboot.mapper.HouseRebuildDeviceMapper;
import com.sf.gis.boot.rcboot.service.IHouseRebuildDeviceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 段朝旭
 * @since 2020-09-23
 */
@Service
public class HouseRebuildDeviceServiceImpl extends ServiceImpl<HouseRebuildDeviceMapper, HouseRebuildDevice> implements IHouseRebuildDeviceService {

    @Override
    public List<HouseRebuildDevice> getDeviceByAoiId(String aoi_id) {
        QueryWrapper<HouseRebuildDevice> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(HouseRebuildDevice::getHouseRebuildAoiId, aoi_id);
        List<HouseRebuildDevice> list = this.list(queryWrapper);
        return list;
    }

}
