package com.sf.gis.boot.rcboot.shiro.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sf.gis.boot.rcboot.shiro.entity.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 系统用户表 Mapper 接口
 * </p>
 */
@Repository
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {

}
