package com.sf.gis.boot.rcboot.constants;

import com.google.gson.Gson;

/**
 * @author 80004819
 * @ClassName:
 * @Description:
 * @date 2020年09月14日 15:15:13
 */
public class Common {

    /**
     * 公共json转换类
     */
    public static final Gson GSON = new Gson();
}
