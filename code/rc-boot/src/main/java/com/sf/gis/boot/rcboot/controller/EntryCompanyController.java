package com.sf.gis.boot.rcboot.controller;


import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 入驻商家表 前端控制器
 * </p>
 *
 * @author 段朝旭
 * @since 2020-09-11
 */
@RestController
@RequestMapping("/entry-company")
@Api(tags = "入驻商家")
public class EntryCompanyController {



}
