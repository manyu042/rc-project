package com.sf.gis.boot.rcboot.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 迁入迁出情况表 前端控制器
 * </p>
 *
 * @author 段朝旭
 * @since 2020-09-10
 */
@RestController
@RequestMapping("/move-in-out")
@Api(tags = "迁入迁出")
public class MoveInOutController {

}
