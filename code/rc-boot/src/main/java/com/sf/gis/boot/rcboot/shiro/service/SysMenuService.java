package com.sf.gis.boot.rcboot.shiro.service;



import com.baomidou.mybatisplus.extension.service.IService;
import com.sf.gis.boot.rcboot.shiro.entity.SysMenu;

import java.util.List;

/**
 * <p>
 * 菜单权限表 服务类
 * </p>
 */
public interface SysMenuService extends IService<SysMenu> {

    List<SysMenu> findUserPermissions(String username);

}
