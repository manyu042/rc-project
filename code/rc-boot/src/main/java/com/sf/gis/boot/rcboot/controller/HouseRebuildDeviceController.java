package com.sf.gis.boot.rcboot.controller;


import cn.hutool.core.util.StrUtil;
import com.sf.gis.boot.rcboot.entity.HouseRebuildDevice;
import com.sf.gis.boot.rcboot.service.IHouseRebuildDeviceService;
import com.sf.gis.boot.rcboot.service.IHouseRebuildService;
import com.sf.gis.boot.rcboot.util.JsonResponse;
import com.sf.gis.boot.rcboot.vo.CommunityStatistVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.sf.gis.boot.rcboot.util.JsonResponse.STATUS_FAILED;
import static com.sf.gis.boot.rcboot.util.JsonResponse.STATUS_SUCCESS;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 段朝旭
 * @since 2020-09-23
 */
@RestController
@RequestMapping("/house-rebuild-device")
@Slf4j
@Api(tags = "棚屋改造设备列表")
public class HouseRebuildDeviceController {

    @Autowired
    private IHouseRebuildDeviceService iHouseRebuildDeviceService;

    /**
     * 获取设备树形结构数据
     *
     * @return
     */
    @GetMapping("/getDeviceByAoiId")
    @ApiOperation("根据aoiId获取棚屋改造设备列表")
    public JsonResponse getDeviceByAoiId(@RequestParam("aoi_id") String aoi_id) {
        JsonResponse result = new JsonResponse();
        try {
            if (StrUtil.isBlank(aoi_id)) {
                result.setData(STATUS_FAILED, "aoi_id不能为空");
                return result;
            }
            List<HouseRebuildDevice> list = iHouseRebuildDeviceService.getDeviceByAoiId(aoi_id);
            result.setData(STATUS_SUCCESS, list);
            return result;
        } catch (Exception e) {
            log.error("error", e);
            result.setData(STATUS_FAILED, null);
            return result;
        }
    }

}
