package com.sf.gis.boot.rcboot.shiro.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sf.gis.boot.rcboot.shiro.entity.SysUserRole;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 用户角色关系表 Mapper 接口
 * </p>
 */
@Repository
@Mapper
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
