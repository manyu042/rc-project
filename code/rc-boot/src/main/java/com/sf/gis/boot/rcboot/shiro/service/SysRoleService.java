package com.sf.gis.boot.rcboot.shiro.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.sf.gis.boot.rcboot.shiro.entity.SysRole;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 系统角色表 服务类
 * </p>
 */
public interface SysRoleService extends IService<SysRole> {

    List<SysRole> findUserRole(String username);


}
