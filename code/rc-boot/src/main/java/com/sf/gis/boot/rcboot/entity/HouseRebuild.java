package com.sf.gis.boot.rcboot.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 棚屋改造表
 * </p>
 *
 * @author 段朝旭
 * @since 2020-09-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "HouseRebuild对象", description = "棚屋改造表")
@TableName("house_rebuild")
public class HouseRebuild implements Serializable {

    private static final long serialVersionUID = 1L;


    @TableId(value = "aoi_id")
    private String aoiId;

    @TableField("aoi")
    private String aoi;

    @TableField("shape")
    private String shape;

    @TableField(value = "shape_text")
    private String shapeText;

    @TableField("create_time")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;


    @ApiModelProperty(value = "建设中")
    @TableField("build_ing")
    private Integer buildIng;

    @ApiModelProperty(value = "建设已完成")
    @TableField("build_finish")
    private Integer buildFinish;

    @ApiModelProperty(value = "建设未完成")
    @TableField("build_no_finish")
    private Integer buildNoFinish;

    @ApiModelProperty(value = "写字楼")
    @TableField("office_building")
    private Integer officeBuilding;

    @ApiModelProperty(value = "回迁房")
    @TableField("return_house")
    private Integer returnHouse;

    @ApiModelProperty(value = "学校")
    @TableField("school")
    private Integer school;

    @ApiModelProperty(value = "路桥")
    @TableField("road_bridge")
    private Integer roadBridge;

    @ApiModelProperty(value = "已安置")
    @TableField("ready_settled")
    private Integer readySettled;

    @ApiModelProperty(value = "未安置")
    @TableField("no_settled")
    private Integer noSettled;

    @ApiModelProperty(value = "待安置")
    @TableField("wait_settled")
    private Integer waitSettled;

    @ApiModelProperty(value = "餐饮")
    @TableField("restaurant")
    private Integer restaurant;

    @ApiModelProperty(value = "购物")
    @TableField("shopping")
    private Integer shopping;

    @ApiModelProperty(value = "金融")
    @TableField("finance")
    private Integer finance;

    @ApiModelProperty(value = "房产")
    @TableField("house_peoperty")
    private Integer housePeoperty;

    @ApiModelProperty(value = "体育")
    @TableField("sport")
    private Integer sport;

    @ApiModelProperty(value = "医疗")
    @TableField("doctor_treat")
    private Integer doctorTreat;


    @ApiModelProperty(value = "教育")
    @TableField("education")
    private Integer education;

    @ApiModelProperty(value = "其他")
    @TableField("other")
    private Integer other;

    @ApiModelProperty(value = "视频url")
    @TableField("video_url")
    private String videoUrl;

    @ApiModelProperty(value = "相机坐标，角度")
    @TableField(value = "cameraCoordinate", exist = false)
    private CameraCoordinate cameraCoordinate;

    @ApiModelProperty(value = "定位坐标")
    @TableField(value = "positionCoordinate", exist = false)
    private PositionCoordinate positionCoordinate;

    @ApiModelProperty(value = "相机经度")
    @TableField("x")
    @JsonIgnore
    private BigDecimal x;

    @ApiModelProperty(value = "相机纬度")
    @TableField("y")
    @JsonIgnore
    private BigDecimal y;

    @ApiModelProperty(value = "相机z轴")
    @TableField("z")
    @JsonIgnore
    private BigDecimal z;

    @ApiModelProperty(value = "相机俯仰角")
    @TableField("course")
    @JsonIgnore
    private BigDecimal course;

    @ApiModelProperty(value = "相机方位角")
    @TableField("alpha")
    @JsonIgnore
    private BigDecimal alpha;

    @ApiModelProperty(value = "相机滚转角")
    @TableField("roll")
    @JsonIgnore
    private BigDecimal roll;

    @ApiModelProperty(value = "定位经度")
    @TableField("pos_x")
    @JsonIgnore
    private BigDecimal posX;

    @ApiModelProperty(value = "定位纬度")
    @TableField("pos_y")
    @JsonIgnore
    private BigDecimal posY;

    @ApiModelProperty(value = "定位z轴")
    @TableField("pos_z")
    @JsonIgnore
    private BigDecimal posZ;

//    @ApiModelProperty(value = "棚屋改造设备列表")
//    @TableField(exist = false)
//    private List<HouseRebuildDevice> deviceList = new ArrayList<>();


}
