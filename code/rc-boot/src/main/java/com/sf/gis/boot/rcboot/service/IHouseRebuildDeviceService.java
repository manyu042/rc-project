package com.sf.gis.boot.rcboot.service;

import com.sf.gis.boot.rcboot.entity.HouseRebuildDevice;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 段朝旭
 * @since 2020-09-23
 */
public interface IHouseRebuildDeviceService extends IService<HouseRebuildDevice> {

    List<HouseRebuildDevice> getDeviceByAoiId(String aoi_id);
    
}
