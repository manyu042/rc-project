package com.sf.gis.boot.rcboot.shiro.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sf.gis.boot.rcboot.shiro.entity.SysMenu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 菜单权限表 Mapper 接口
 * </p>
 */
@Repository
@Mapper
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    List<SysMenu> findUserPermissions(@Param("username") String username);


}
