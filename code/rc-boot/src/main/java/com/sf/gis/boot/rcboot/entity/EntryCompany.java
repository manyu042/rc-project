package com.sf.gis.boot.rcboot.entity;

import java.time.LocalDateTime;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 入驻商家表
 * </p>
 *
 * @author 段朝旭
 * @since 2020-09-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "EntryCompany对象", description = "入驻商家表")
public class EntryCompany implements Serializable {

    private static final long serialVersionUID = 1L;


    @TableId(value = "id")
    private Integer id;

    @TableField(value = "aoi_id")
    private String aoiId;

    @TableField("aoi")
    private String aoi;

    @TableField("shape")
    private String shape;

    @ApiModelProperty(value = "标准地址")
    @TableField("standard_address")
    private String standardAddress;

    @ApiModelProperty(value = "商户名称")
    @TableField("name")
    private String name;

    @ApiModelProperty(value = "入驻时间")
    @TableField("entry_date")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime entryDate;


}
