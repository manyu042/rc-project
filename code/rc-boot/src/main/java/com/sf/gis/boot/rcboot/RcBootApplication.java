package com.sf.gis.boot.rcboot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@MapperScan(basePackages = {"com.sf.gis.boot.rcboot.mapper","com.sf.gis.boot.rcboot.shiro.mapper"})
@EnableSwagger2
@EnableScheduling
@EnableTransactionManagement
@ServletComponentScan
public class RcBootApplication  extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(RcBootApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(RcBootApplication.class, args);
    }

}
