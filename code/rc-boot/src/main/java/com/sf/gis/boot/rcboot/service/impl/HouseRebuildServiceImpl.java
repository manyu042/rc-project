package com.sf.gis.boot.rcboot.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sf.gis.boot.rcboot.entity.*;
import com.sf.gis.boot.rcboot.mapper.EntryCompanyMapper;
import com.sf.gis.boot.rcboot.mapper.HouseRebuildDeviceMapper;
import com.sf.gis.boot.rcboot.mapper.HouseRebuildMapper;
import com.sf.gis.boot.rcboot.mapper.MoveInOutMapper;
import com.sf.gis.boot.rcboot.service.IHouseRebuildService;
import com.sf.gis.boot.rcboot.vo.HouseRebuildVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.stream.Collectors;

/**
 * <p>
 * 棚屋改造表 服务实现类
 * </p>
 *
 * @author 段朝旭
 * @since 2020-09-11
 */
@Service
public class HouseRebuildServiceImpl extends ServiceImpl<HouseRebuildMapper, HouseRebuild> implements IHouseRebuildService {


    @Autowired
    private HouseRebuildMapper houseRebuildMapper;

    @Autowired
    private MoveInOutMapper moveInOutMapper;

    @Autowired
    private EntryCompanyMapper entryCompanyMapper;

    @Autowired
    private HouseRebuildDeviceMapper houseRebuildDeviceMapper;

    @Override
    public HouseRebuild getByAoiId(String aoi_id) {
        HouseRebuild houseRebuild = houseRebuildMapper.getByAoiId(aoi_id);
        this.setCoord(houseRebuild);
      //  this.setDeviceList(houseRebuild);
        return houseRebuild;
    }


    @Override
    public List<HouseRebuild> getHouseByParentAoiId(String parentAoiId) {
        List<HouseRebuild> houseRebuildList = houseRebuildMapper.getHouseByParentAoiId(parentAoiId);
        houseRebuildList.forEach(houseRebuild -> {
            this.setCoord(houseRebuild);
        //    this.setDeviceList(houseRebuild);
        });
        return houseRebuildList;
    }

    @Override
    public LinkedHashMap<String, Object> staticHouseReBuild(String aoiId) {
        //结果集
        LinkedHashMap<String, Object> result = new LinkedHashMap<>();

        ConcurrentSkipListMap<Integer, Integer> skipListMap = initMonthMap();
        //查询房屋改造要统计的数据
        LinkedHashMap<String, Object> staticHouseReBuildMap = houseRebuildMapper.staticHouseReBuild(aoiId);
        result.put("staticData", staticHouseReBuildMap);
        //根据aoiId 查询迁入情况
        List<LinkedHashMap<String, Object>> moveInList = moveInOutMapper.selectMoveInGroupByMonth(aoiId);
        List<Map<String, Integer>> moveInItemList = replMapKV(skipListMap, moveInList);
        result.put("moveInList", moveInItemList);

        //根据aoiId查询迁出情况
        skipListMap = initMonthMap();
        List<LinkedHashMap<String, Object>> moveOutList = moveInOutMapper.selectMoveOutGroupByMonth(aoiId);
        List<Map<String, Integer>> moveOutItemList = replMapKV(skipListMap, moveOutList);
        result.put("moveOutList", moveOutItemList);


        //根据aoiId 查询最新入驻商家
        QueryWrapper<EntryCompany> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(EntryCompany::getAoiId, aoiId).orderByDesc(EntryCompany::getEntryDate).last("limit 5");
        List<EntryCompany> entryCompanies = entryCompanyMapper.selectList(queryWrapper);
        result.put("entryCompanies", entryCompanies);
        return result;
    }


    /**
     * 替换map的kv
     *
     * @param skipListMap
     * @param moveOutList
     * @return
     */
    private List<Map<String, Integer>> replMapKV(ConcurrentSkipListMap<Integer, Integer> skipListMap, List<LinkedHashMap<String, Object>> moveOutList) {
        for (LinkedHashMap<String, Object> map : moveOutList) {
            if (map.get("x") != null && map.get("y") != null) {
                int x = Integer.parseInt(map.get("x").toString());
                int y = Integer.parseInt(map.get("y").toString());
                if (skipListMap.containsKey(x)) {
                    skipListMap.replace(x, y);
                }
            }
        }
        List<Map<String, Integer>> itemList = new ArrayList(skipListMap.size());
        for (Map.Entry<Integer, Integer> map : skipListMap.entrySet()) {
            Map<String, Integer> itemMap = new HashMap<>();
            itemMap.put("label", map.getKey());
            itemMap.put("value", map.getValue());
            itemList.add(itemMap);
        }
        return itemList;
    }


    /**
     * 初始化一个1月份到当前月份的map
     *
     * @return
     */
    public ConcurrentSkipListMap<Integer, Integer> initMonthMap() {
        //初始化一个当年1月到当前月的map
        int monthValue = LocalDate.now().getMonthValue();
        ConcurrentSkipListMap<Integer, Integer> skipListMap = new ConcurrentSkipListMap<>(Integer::compareTo);
        for (int i = 1; i <= monthValue; i++) {
            skipListMap.put(i, 0);
        }
        return skipListMap;
    }


    @Override
    public HouseRebuildVo getHouseTreeByAoiId(String aoi_id) {
        //获取当前节点
        HouseRebuild root = this.getByAoiId(aoi_id);
        HouseRebuildVo rootVo = new HouseRebuildVo();
        if (root != null) {
            BeanUtils.copyProperties(root, rootVo);
            rootVo = getHouseTree(rootVo);
        }
        //转换成vo
        return rootVo;
    }


    /**
     * 组装棚屋改造树形结构
     *
     * @param rootVo
     * @return
     */
    private HouseRebuildVo getHouseTree(HouseRebuildVo rootVo) {
        String aoiId = rootVo.getAoiId();
        List<HouseRebuildVo> children = this.getHouseByParentAoiId(aoiId).stream().map(x -> {
                    HouseRebuildVo houseRebuildVo = new HouseRebuildVo();
                    BeanUtils.copyProperties(x, houseRebuildVo);
                    return houseRebuildVo;
                }
        ).collect(Collectors.toList());

        if (!CollectionUtils.isEmpty(children)) {
            rootVo.getChildren().addAll(children);
            for (HouseRebuildVo node : children) {
                getHouseTree(node);
            }
        }
        return rootVo;
    }

    /**
     * 设置坐标角度信息
     *
     * @param houseRebuild
     */
    private void setCoord(HouseRebuild houseRebuild) {

        //设置真实定位坐标
        BigDecimal posX = houseRebuild.getPosX();
        BigDecimal posY = houseRebuild.getPosY();
        BigDecimal posZ = houseRebuild.getPosZ();
        PositionCoordinate positionCoordinate = new PositionCoordinate(posX, posY, posZ);
        houseRebuild.setPositionCoordinate(positionCoordinate);

        //设置相机定位坐标和角度
        BigDecimal x = houseRebuild.getX();
        BigDecimal y = houseRebuild.getY();
        BigDecimal z = houseRebuild.getZ();
        BigDecimal course = houseRebuild.getCourse();
        BigDecimal alpha = houseRebuild.getAlpha();
        BigDecimal roll = houseRebuild.getRoll();
        CameraCoordinate cameraCoordinate = new CameraCoordinate(x, y, z, course, alpha, roll);
        houseRebuild.setCameraCoordinate(cameraCoordinate);
    }


//    /**
//     * 根据棚屋改造的aoiid查询相应的设备
//     *
//     * @param houseRebuild
//     */
//    private void setDeviceList(HouseRebuild houseRebuild) {
//        QueryWrapper<HouseRebuildDevice> queryWrapper = new QueryWrapper<>();
//        queryWrapper.lambda().eq(HouseRebuildDevice::getHouseRebuildAoiId, houseRebuild.getAoiId());
//        List<HouseRebuildDevice> houseRebuildDevices = houseRebuildDeviceMapper.selectList(queryWrapper);
//        houseRebuild.setDeviceList(houseRebuildDevices);
//    }

}
