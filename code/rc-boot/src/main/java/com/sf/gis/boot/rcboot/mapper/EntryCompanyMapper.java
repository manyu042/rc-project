package com.sf.gis.boot.rcboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sf.gis.boot.rcboot.entity.EntryCompany;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 入驻商家表 Mapper 接口
 * </p>
 *
 * @author 段朝旭
 * @since 2020-09-11
 */
@Mapper
@Repository
public interface EntryCompanyMapper extends BaseMapper<EntryCompany> {

}
