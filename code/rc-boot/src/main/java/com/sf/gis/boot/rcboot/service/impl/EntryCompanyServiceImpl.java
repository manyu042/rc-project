package com.sf.gis.boot.rcboot.service.impl;

import com.sf.gis.boot.rcboot.entity.EntryCompany;
import com.sf.gis.boot.rcboot.mapper.EntryCompanyMapper;
import com.sf.gis.boot.rcboot.service.IEntryCompanyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 入驻商家表 服务实现类
 * </p>
 *
 * @author 段朝旭
 * @since 2020-09-11
 */
@Service
public class EntryCompanyServiceImpl extends ServiceImpl<EntryCompanyMapper, EntryCompany> implements IEntryCompanyService {

}
