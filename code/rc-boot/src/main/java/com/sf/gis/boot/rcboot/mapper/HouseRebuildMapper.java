package com.sf.gis.boot.rcboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sf.gis.boot.rcboot.entity.HouseRebuild;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * <p>
 * 棚屋改造表 Mapper 接口
 * </p>
 *
 * @author 段朝旭
 * @since 2020-09-11
 */
@Mapper
@Repository
public interface HouseRebuildMapper extends BaseMapper<HouseRebuild> {

    LinkedHashMap<String,Object> staticHouseReBuild(String aoiId);

    HouseRebuild getByAoiId(String aoi_id);

    List<HouseRebuild> getHouseByParentAoiId(String parentAoiId);
}
