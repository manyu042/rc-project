package com.sf.gis.boot.rcboot.controller;

import com.sf.gis.boot.rcboot.shiro.JWTUtil;
import com.sf.gis.boot.rcboot.util.JsonResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 80004819
 * @ClassName:
 * @Description:
 * @date 2020年09月11日 17:38:09
 */
@RestController
@RequestMapping("/jwt")
@Slf4j
@Api(tags = "JWT权限controller")
public class JwtTokenController {


    /**
     * 模拟登陆接口获取到token，有效期为30分钟
     *
     * @return
     */
    @GetMapping("/getToken")
    @ApiOperation("获取token")
    public JsonResponse getToken() {
        try {
            //String token = JWTUtil.createToken(new User("admin", "123456"));
            String token = JWTUtil.sign("admin", "123456", false);
            return JsonResponse.ok(JsonResponse.STATUS_SUCCESS, token);
        } catch (Exception e) {
            log.error("error", e);
            return JsonResponse.error("获取token失败");
        }
    }

}
