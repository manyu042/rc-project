package com.sf.gis.boot.rcboot.controller;


import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.github.benmanes.caffeine.cache.Cache;
import com.sf.gis.boot.rcboot.entity.HouseRebuild;
import com.sf.gis.boot.rcboot.service.IHouseRebuildService;
import com.sf.gis.boot.rcboot.util.JsonResponse;
import com.sf.gis.boot.rcboot.vo.HouseRebuildVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedHashMap;
import java.util.Optional;

import static com.sf.gis.boot.rcboot.util.JsonResponse.STATUS_FAILED;
import static com.sf.gis.boot.rcboot.util.JsonResponse.STATUS_SUCCESS;

/**
 * <p>
 * 棚屋改造表 前端控制器
 * </p>
 *
 * @author 段朝旭
 * @since 2020-09-11
 */
@RestController
@RequestMapping("/house-rebuild")
@Api(tags = "棚屋改造")
@Slf4j
public class HouseRebuildController {


    @Autowired
    private IHouseRebuildService iHouseRebuildService;

    @Autowired
    private Cache<String, Object> cache;


    @ApiOperation("根据aoi_id查询棚屋改造数据")
    @GetMapping("/getHouseByAoiId")
    public JsonResponse getHouseByAoiId(@RequestParam("aoi_id") String aoi_id) {
        try {
            HouseRebuild aoiStatist = iHouseRebuildService.getByAoiId(aoi_id);
            //查询人口统计总数，法人总数，房屋总数，房屋使用情况总数
            LinkedHashMap<String, Object> resultMap = Optional.ofNullable(iHouseRebuildService.staticHouseReBuild(aoi_id)).orElse(new LinkedHashMap<>());
            resultMap.put("aoiStatist", aoiStatist);
            return JsonResponse.ok(STATUS_SUCCESS, resultMap);
        } catch (Exception e) {
            log.error("error", e);
            return JsonResponse.error("error");
        }
    }


    /**
     * 根据aoiId获取棚屋改造树形结构
     *
     * @return
     */
    @GetMapping("/getHouseTreeByAoiId")
    @ApiOperation("根据aoiId获取棚屋改造树形结构")
    public JsonResponse getHouseTreeByAoiId(@RequestParam("aoi_id") String aoi_id) {
        JsonResponse result = new JsonResponse();
        try {
            if (StrUtil.isBlank(aoi_id)) {
                result.setData(STATUS_FAILED, "aoi_id不能为空");
                return result;
            }
            HouseRebuildVo resultTree = iHouseRebuildService.getHouseTreeByAoiId(aoi_id);
            result.setData(STATUS_SUCCESS, resultTree);
            return result;
        } catch (Exception e) {
            log.error("error", e);
            result.setData(STATUS_FAILED, null);
            return result;
        }
    }


    @GetMapping("/getHouseByAoiIdWithCache")
    @ApiOperation("根据aoi_id查询棚屋改造数据(带有缓存~~过期时间20s)")
    public JsonResponse getHouseByAoiIdWithCache(@RequestParam("aoi_id") String aoi_id) {
        try {
            //从缓存中获取棚屋改造数据
            Object houseRebuildObj = cache.get("houseRebuild-" + aoi_id, s -> iHouseRebuildService.getByAoiId(aoi_id));
            HouseRebuild aoiStatist = ObjectUtil.isNotEmpty(houseRebuildObj) ? (HouseRebuild) houseRebuildObj : null;
            //从缓存中获取棚屋改造统计总数，法人总数，房屋总数，房屋使用情况总数
            Object linkedHashMapObj = cache.get("houseRebuildStatic-" + aoi_id, s -> Optional.ofNullable(iHouseRebuildService.staticHouseReBuild(aoi_id)).orElse(new LinkedHashMap<>()));
            LinkedHashMap<String, Object> resultMap = (LinkedHashMap<String, Object>) linkedHashMapObj;
            resultMap.put("aoiStatist", aoiStatist);
            return JsonResponse.ok(STATUS_SUCCESS, resultMap);
        } catch (Exception e) {
            log.error("error", e);
            return JsonResponse.error("error");
        }
    }


    /**
     * 根据aoiId获取棚屋改造树形结构
     *
     * @return
     */
    @GetMapping("/getHouseTreeByAoiIdWithCache")
    @ApiOperation("根据aoiId获取棚屋改造树形结构(带有缓存~~过期时间20s)")
    public JsonResponse getHouseTreeByAoiIdWithCache(@RequestParam("aoi_id") String aoi_id) {
        JsonResponse result = new JsonResponse();
        try {
            if (StrUtil.isBlank(aoi_id)) {
                result.setData(STATUS_FAILED, "aoi_id不能为空");
                return result;
            }
            //从缓存中获取房屋改造树形结构数据
            Object o = cache.get("houseRebuildTree-" + aoi_id, s -> iHouseRebuildService.getHouseTreeByAoiId(aoi_id));
            HouseRebuildVo resultTree = ObjectUtil.isNotEmpty(o)?(HouseRebuildVo)o:null;
            result.setData(STATUS_SUCCESS, resultTree);
            return result;
        } catch (Exception e) {
            log.error("error", e);
            result.setData(STATUS_FAILED, null);
            return result;
        }
    }


}
