package com.sf.gis.boot.rcboot.shiro.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sf.gis.boot.rcboot.shiro.entity.SysMenu;
import com.sf.gis.boot.rcboot.shiro.mapper.SysMenuMapper;
import com.sf.gis.boot.rcboot.shiro.service.SysMenuService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 菜单权限表 服务实现类
 * </p>
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements SysMenuService {



    @Override
    public List<SysMenu> findUserPermissions(String username) {
        return this.baseMapper.findUserPermissions(username);
    }


}
