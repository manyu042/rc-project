package com.sf.gis.boot.rcboot.vo;

import com.sf.gis.boot.rcboot.entity.CommunityStatist;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 80004819
 * @ClassName:
 * @Description:
 * @date 2020年09月16日 09:16:27
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class CommunityStatistVo extends CommunityStatist {

    private List<CommunityStatistVo> children = new ArrayList<>();
}
