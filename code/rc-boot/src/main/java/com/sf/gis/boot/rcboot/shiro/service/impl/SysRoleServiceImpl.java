package com.sf.gis.boot.rcboot.shiro.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sf.gis.boot.rcboot.shiro.entity.SysRole;
import com.sf.gis.boot.rcboot.shiro.entity.SysRoleMenu;
import com.sf.gis.boot.rcboot.shiro.mapper.SysRoleMapper;
import com.sf.gis.boot.rcboot.shiro.service.SysRoleMenuService;
import com.sf.gis.boot.rcboot.shiro.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 系统角色表 服务实现类
 * </p>
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {



    @Override
    public List<SysRole> findUserRole(String username) {
        return this.baseMapper.findUserRole(username);
    }





}
