package com.sf.gis.boot.rcboot.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * @author 80004819
 * @ClassName:
 * @Description:
 * @date 2020年09月11日 10:39:36
 */
@Configuration
public class Swagger2Config {

    // Swagger 容器初始化配置
    @Bean
    public Docket newsApi() {
        return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).select()
                .apis(RequestHandlerSelectors.basePackage("com.sf.gis.boot.rcboot.controller"))
                .paths(PathSelectors.any()).build()
                .enable(true);
    }

    // 文档信息描述
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("睿呈项目").description("接口文档")
                .termsOfServiceUrl("").license("Version 1.0").licenseUrl("#")
                .version("1.0").build();
    }

}
