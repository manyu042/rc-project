package com.sf.gis.boot.rcboot.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@AllArgsConstructor
@NoArgsConstructor
public class JsonResponse {

    public static final int STATUS_SUCCESS = 200;
    public static final int STATUS_FAILED = 500;

    private int status;
    private String code;
    private Object result;
    private int total;
    private String message;

    public JsonResponse(int status, Object result, int total, String message) {
        this.status = status;
        this.result = result;
        this.total = total;
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setData(int status, Object result) {
        this.setStatus(status);
        this.setResult(result);
    }

    public String toJSON() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public void setData(int status, Object result, String message) {
        this.setStatus(status);
        this.setResult(result);
        this.setMessage(message);
    }

    public void success(Object data) {
        this.setData(STATUS_SUCCESS, data);
    }

    public void fail(Object data) {
        this.setData(STATUS_FAILED, data);
    }

    public static <T> JsonResponse error(int status, T data, int total, String message) {
        return new JsonResponse(status, data, total, message);
    }

    public static JsonResponse error(String message) {
        return error(STATUS_FAILED, null, STATUS_FAILED , message);
    }

    public static <T> JsonResponse ok(int status, T res, int total, String message) {
        return new JsonResponse(status, res, total, message);
    }

    public static <T> JsonResponse ok(T data, String message) {
        return ok(STATUS_SUCCESS, data, STATUS_SUCCESS , message);
    }

    public static <T> JsonResponse ok(int status, T data){
        return ok(STATUS_SUCCESS,data,STATUS_SUCCESS,null);
    }

    public static void responseOutWithJson(HttpServletResponse response, Object responseObject) {
        // 将实体对象转换为JSON Object转换
        String responseJSONObject = new Gson().toJson(responseObject);
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.append(responseJSONObject);
        } catch (IOException e) {
//            logger.error("写入错误", e);
            e.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }
}
