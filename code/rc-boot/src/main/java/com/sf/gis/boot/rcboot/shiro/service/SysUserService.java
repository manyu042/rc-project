package com.sf.gis.boot.rcboot.shiro.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sf.gis.boot.rcboot.shiro.entity.SysUser;

/**
 * <p>
 * 系统用户表 服务类
 * </p>
 */
public interface SysUserService extends IService<SysUser> {


    SysUser findByNamePassword(String username, String password);
}
