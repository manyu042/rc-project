package com.sf.gis.boot.rcboot.mapper;

import com.sf.gis.boot.rcboot.entity.HouseRebuildDevice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 段朝旭
 * @since 2020-09-23
 */
@Mapper
@Repository
public interface HouseRebuildDeviceMapper extends BaseMapper<HouseRebuildDevice> {

}
