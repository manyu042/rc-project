package com.sf.gis.boot.rcboot.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * @author 80004819
 * @ClassName:
 * @Description:
 * @date 2020年09月24日 09:50:37
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "PositionCoordinate对象", description = "aoi坐标")
@AllArgsConstructor
@NoArgsConstructor
public class PositionCoordinate {

    @ApiModelProperty(value = "经度")
    private BigDecimal x;

    @ApiModelProperty(value = "纬度")
    private BigDecimal y;

    @ApiModelProperty(value = "z轴")
    private BigDecimal z;
}
