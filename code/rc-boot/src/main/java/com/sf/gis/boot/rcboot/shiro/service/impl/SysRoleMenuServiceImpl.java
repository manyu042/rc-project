package com.sf.gis.boot.rcboot.shiro.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.sf.gis.boot.rcboot.shiro.entity.SysRoleMenu;
import com.sf.gis.boot.rcboot.shiro.mapper.SysRoleMenuMapper;
import com.sf.gis.boot.rcboot.shiro.service.SysRoleMenuService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements SysRoleMenuService {

    @Override
    public void deleteByRoleKey(String roleKey) {

        QueryWrapper<SysRoleMenu> deleteWrapper = new QueryWrapper<>();
        deleteWrapper.eq("role_key", roleKey);
        this.baseMapper.delete(deleteWrapper);
    }

    @Override
    public void deleteByMenuIds(List<Long> menuIds) {

        QueryWrapper<SysRoleMenu> deleteWrapper = new QueryWrapper<>();
        deleteWrapper.in("menu_id", menuIds);
        this.baseMapper.delete(deleteWrapper);
    }

    @Override
    public List<SysRoleMenu> listByRoleKey(String roleKey) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("role_key", roleKey);
        return this.baseMapper.selectList(queryWrapper);
    }
}
