package com.sf.gis.boot.rcboot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sf.gis.boot.rcboot.entity.HouseRebuild;
import com.sf.gis.boot.rcboot.vo.HouseRebuildVo;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * <p>
 * 棚屋改造表 服务类
 * </p>
 *
 * @author 段朝旭
 * @since 2020-09-11
 */
public interface IHouseRebuildService extends IService<HouseRebuild> {

    LinkedHashMap<String, Object> staticHouseReBuild(String aoiId);

    HouseRebuild getByAoiId(String aoi_id);

    HouseRebuildVo getHouseTreeByAoiId(String aoi_id);

    List<HouseRebuild> getHouseByParentAoiId(String parentAoiId);


}
