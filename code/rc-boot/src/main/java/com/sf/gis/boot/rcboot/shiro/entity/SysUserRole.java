package com.sf.gis.boot.rcboot.shiro.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 用户角色关系表
 * </p>
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysUserRole extends Model<SysUserRole> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    private Long uid;

    /**
     * 账号
     */
    private String username;

    /**
     * 角色标识
     */
    private String roleKey;


    @Override
    protected Serializable pkVal() {
        return null;
    }

}
