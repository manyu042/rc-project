package com.sf.gis.boot.rcboot.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 80004819
 * @ClassName:
 * @Description:
 * @date 2020年09月11日 10:38:09
 */
@Configuration
public class MybatisPlusConfiug {


    /**
     * mybatis-plus 分页插件
     *
     * @return
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }


}
