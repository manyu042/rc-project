package com.sf.gis.boot.rcboot.shiro.service;



import com.baomidou.mybatisplus.extension.service.IService;
import com.sf.gis.boot.rcboot.shiro.entity.SysRoleMenu;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 */
public interface SysRoleMenuService extends IService<SysRoleMenu> {

    void deleteByRoleKey(String roleKey);

    void deleteByMenuIds(List<Long> menuIds);

    List<SysRoleMenu> listByRoleKey(String roleId);
}
