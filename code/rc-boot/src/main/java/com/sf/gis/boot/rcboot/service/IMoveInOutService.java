package com.sf.gis.boot.rcboot.service;

import com.sf.gis.boot.rcboot.entity.MoveInOut;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 迁入迁出情况表 服务类
 * </p>
 *
 * @author 段朝旭
 * @since 2020-09-10
 */
public interface IMoveInOutService extends IService<MoveInOut> {

}
