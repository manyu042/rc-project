package com.sf.gis.boot.rcboot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sf.gis.boot.rcboot.entity.CommunityStatist;
import com.sf.gis.boot.rcboot.vo.CommunityStatistVo;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * <p>
 * 社区管理表 服务类
 * </p>
 *
 * @author 段朝旭
 * @since 2020-09-11
 */
public interface ICommunityStatistService extends IService<CommunityStatist> {


    CommunityStatist  getByAoiId(String aoiId);

    LinkedHashMap<String,Object>  staticCommunity(String aoiId);

    CommunityStatistVo getCommunityTreeByAoiId(String aoiId);

    List<CommunityStatist> getCommunityByParentAoiId(String parentAoiId);

}
