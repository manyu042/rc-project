package com.sf.gis.boot.rcboot.shiro.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sf.gis.boot.rcboot.shiro.entity.SysUserRole;

import java.util.List;

/**
 * <p>
 * 用户角色关系表 服务类
 * </p>
 */
public interface SysUserRoleService extends IService<SysUserRole> {

    void deleteUserRolesByUsername(String username);

    List<SysUserRole> listByUsername(String userId);
}
