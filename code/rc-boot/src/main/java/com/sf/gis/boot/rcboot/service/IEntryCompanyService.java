package com.sf.gis.boot.rcboot.service;

import com.sf.gis.boot.rcboot.entity.EntryCompany;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 入驻商家表 服务类
 * </p>
 *
 * @author 段朝旭
 * @since 2020-09-11
 */
public interface IEntryCompanyService extends IService<EntryCompany> {

}
