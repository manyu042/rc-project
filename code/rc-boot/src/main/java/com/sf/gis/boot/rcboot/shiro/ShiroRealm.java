package com.sf.gis.boot.rcboot.shiro;

import cn.hutool.core.util.StrUtil;
import com.sf.gis.boot.rcboot.shiro.entity.SysMenu;
import com.sf.gis.boot.rcboot.shiro.entity.SysRole;
import com.sf.gis.boot.rcboot.shiro.entity.SysUser;
import com.sf.gis.boot.rcboot.shiro.service.SysMenuService;
import com.sf.gis.boot.rcboot.shiro.service.SysRoleService;
import com.sf.gis.boot.rcboot.shiro.service.SysUserService;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author 80004819
 * @ClassName:
 * @Description:
 * @date 2020年09月14日 15:33:22
 */
public class ShiroRealm extends AuthorizingRealm {

    @Autowired
    private SysUserService userService;
    @Autowired
    private SysRoleService roleService;
    @Autowired
    private SysMenuService menuService;


    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof JWTToken;
    }


    /**
     * 授权模块，获取用户角色和权限
     *
     * @param principal principal
     * @return AuthorizationInfo 权限信息
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principal) {
//        User user = (User) SecurityUtils.getSubject().getPrincipal();
//        String userName = user.getUsername();
        String username = ShiroUtil.getUsername(principal.toString());

        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();

        // 获取用户角色集
        List<SysRole> roleList = this.roleService.findUserRole(username);
        Set<String> roleSet = roleList.stream().map(role -> role.getRoleName()).collect(Collectors.toSet());
        simpleAuthorizationInfo.setRoles(roleSet);

        // 获取用户权限集
        List<SysMenu> permissionList = this.menuService.findUserPermissions(username);
        Set<String> permissionSet = new HashSet<>();
        for (SysMenu m : permissionList) {
            // 处理用户多权限 用逗号分隔
            permissionSet.addAll(Arrays.asList(m.getPerms().split(",")));
        }
        simpleAuthorizationInfo.setStringPermissions(permissionSet);
        return simpleAuthorizationInfo;
    }

    /**
     * 用户认证
     *
     * @param authenticationToken AuthenticationToken 身份认证 token
     * @return AuthenticationInfo 身份认证信息
     * @throws AuthenticationException 认证相关异常
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {

        // 获取用户输入的用户名和密码
//        String userName = (String) token.getPrincipal();
//        String password = new String((char[]) token.getCredentials());
//
//        if (User.STATUS_LOCK.equals(user.getStatus())) {
//            throw new LockedAccountException("账号已被锁定,请联系管理员！");
//        }

        String token = (String) authenticationToken.getCredentials();
        SysUser sysUser = ShiroUtil.getUserInfo(token);
        String username = sysUser.getUsername();
        String password = sysUser.getPassword();

        if (StrUtil.isBlank(username))
            throw new AuthenticationException("账号为空，token校验不通过");

        // 通过用户名查询用户信息
        SysUser user = this.userService.findByNamePassword(username,password);

        if (user == null)
            throw new AuthenticationException("用户名或密码错误");

//        JsonObject jo = new JsonObject();
//        jo.addProperty("username", username);
//        jo.addProperty("userid", user.getUserId());

        if (!JWTUtil.verify(token, username, password))
            throw new AuthenticationException("token校验不通过");
        // 判断用户状态
        if (null == user.getStatus() || user.getStatus() != 1) {
            throw new AuthenticationException("账号已被冻结,请联系管理员!");
        }
        return new SimpleAuthenticationInfo(token, token, getName());
    }

}
