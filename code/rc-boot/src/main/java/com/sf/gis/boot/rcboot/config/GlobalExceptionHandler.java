package com.sf.gis.boot.rcboot.config;

import com.sf.gis.boot.rcboot.util.JsonResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;

/**
 * @author 80004819
 * @ClassName:
 * @Description:
 * @date 2020年09月12日 14:59:40
 */
@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = {Exception.class})
    @ResponseBody
    public JsonResponse globalException(HttpServletResponse response, Exception ex) {
        return JsonResponse.error(ex.getMessage());
    }

}



