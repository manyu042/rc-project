package com.sf.gis.boot.rcboot.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * @author 80004819
 * @ClassName:
 * @Description:
 * @date 2020年09月17日 20:01:48
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "CameraCoordinate对象", description = "aoi坐标，角度对象")
@AllArgsConstructor
@NoArgsConstructor
public class CameraCoordinate extends  PositionCoordinate{

    @ApiModelProperty(value = "俯仰角")
    private BigDecimal course;

    @ApiModelProperty(value = "方位角")
    private BigDecimal alpha;

    @ApiModelProperty(value = "滚转角")
    @TableField("roll")
    private BigDecimal  roll;

    public CameraCoordinate(BigDecimal x, BigDecimal y, BigDecimal z, BigDecimal course, BigDecimal alpha, BigDecimal roll) {
        super(x, y, z);
        this.course = course;
        this.alpha = alpha;
        this.roll = roll;
    }
}
