package com.sf.gis.boot.rcboot.controller;

import com.sf.gis.boot.rcboot.util.JsonResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 80004819
 * @ClassName:
 * @Description:
 * @date 2020年09月28日 16:17:48
 */
@RestController
@RequestMapping("/service")
@Api(tags = "通用服务接口")
@Slf4j
public class ServiceController {

    @Value("${rc.3dMap-url}")
    private String mapUrl;

    @GetMapping("getRc3dMapUrl")
    @ApiOperation("获取睿呈3dMap服务地址")
    public JsonResponse getRc3dMapUrl() {
        return JsonResponse.ok(JsonResponse.STATUS_SUCCESS, mapUrl);
    }

}
