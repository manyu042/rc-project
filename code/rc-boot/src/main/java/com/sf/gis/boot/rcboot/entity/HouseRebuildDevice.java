package com.sf.gis.boot.rcboot.entity;

import java.math.BigDecimal;
import java.io.Serializable;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 *
 * </p>
 *
 * @author 段朝旭
 * @since 2020-09-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "HouseRebuildDevice对象", description = "棚屋改造设备表")
@TableName("house_rebuild_device")
public class HouseRebuildDevice implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("id")
    private Integer id;

    @TableField("house_rebuild_aoi_id")
    @ApiModelProperty(value = "房屋改造aoiId")
    private String houseRebuildAoiId;

    @TableField("device_type")
    @ApiModelProperty(value = "设备类型")
    private String deviceType;

    @TableField("device_name")
    @ApiModelProperty(value = "设备名称")
    private String deviceName;

    @TableField("video_url")
    @ApiModelProperty(value = "视频地址")
    private String videoUrl;

    @TableField("x")
    @ApiModelProperty(value = "经度")
    private BigDecimal x;

    @TableField("y")
    @ApiModelProperty(value = "纬度")
    private BigDecimal y;

    @TableField("z")
    @ApiModelProperty(value = "高程")
    private BigDecimal z;

    @TableField("create_time")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @TableField("update_time")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;


}
