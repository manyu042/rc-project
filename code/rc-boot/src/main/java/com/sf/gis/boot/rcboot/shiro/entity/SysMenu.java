package com.sf.gis.boot.rcboot.shiro.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 菜单权限表
 * </p>
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysMenu extends Model<SysMenu> {

    private static final long serialVersionUID = 1L;

    public static final String TYPE_MENU = "0";

    public static final String TYPE_BUTTON = "1";

    @TableId(value = "menu_id", type = IdType.AUTO)
    private Long menuId;

    private Long parentId;

    private String name;

    private String url;

    private String perms;

    private String icon;

    private Integer type;

    private Integer orderNum;

    private String component;

    private String componentName;

    private String redirect;

    private Boolean isRoute;

    private Boolean alwaysShow;

    private Boolean isLeaf;

    private Boolean hidden;

    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime createTime;

    private String createBy;

    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime updateTime;

    private String updateBy;


    private transient Long key;
    private transient String title;
    private transient List<SysMenu> children = new ArrayList<>();

    @Override
    protected Serializable pkVal() {
        return this.menuId;
    }

    public Long getKey() {
        return menuId;
    }

    public String getTitle() {
        return name;
    }

    public List<SysMenu> getChildren() {
        if (isLeaf) {
            return null;
        }
        return children;
    }
}
