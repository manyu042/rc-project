package com.sf.gis.boot.rcboot.mapper;

import com.sf.gis.boot.rcboot.entity.HouseRebuild;
import com.sf.gis.boot.rcboot.entity.MoveInOut;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * <p>
 * 迁入迁出情况表 Mapper 接口
 * </p>
 *
 * @author 段朝旭
 * @since 2020-09-10
 */
@Mapper
@Repository
public interface MoveInOutMapper extends BaseMapper<MoveInOut> {

    List<LinkedHashMap<String, Object>> selectMoveInGroupByMonth(@Param("aoiId") String aoiId);

    List<LinkedHashMap<String, Object>> selectMoveOutGroupByMonth(@Param("aoiId") String aoiId);
}
